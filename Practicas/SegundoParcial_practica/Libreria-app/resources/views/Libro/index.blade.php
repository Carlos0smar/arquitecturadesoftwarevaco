<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<h1>Listado de clientes</h1>
<div class="flex flex-col">
    <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
        <div class="overflow-hidden">
          <table class="min-w-full text-left text-sm font-light">
            <thead class="border-b font-medium">
              <tr>
                <th scope="col" class="px-6 py-4">Titulo</th>
                <th scope="col" class="px-6 py-4">editorial</th>
                <th scope="col" class="px-6 py-4">edicion</th>    
                <th scope="col" class="px-6 py-4">pais</th>
                <th scope="col" class="px-6 py-4">precio</th>

              </tr>
            </thead>
            <tbody>
                @foreach ($libros as $libro)
                <tr class="border-b">
                    <td class="whitespace-nowrap px-6 py-4">{{$libro->titulo}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$libro->editorial->nombre}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$libro->edicion}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$libro->pais}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$libro->precio}}</td>
                    <td><a href="{{route('libro.show',$libro->id)}}">Mostrar</a>
                        <a href="{{route('libro.edit',$libro->id)}}">Editar</a></td>
                    <td>
                        <form action="{{route('libro.destroy',$libro->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit">Eliminar</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>  
</body>
</html>


