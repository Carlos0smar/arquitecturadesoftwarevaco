<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<form class="w-full " action="{{route('libro.store')}}" method="POST">
    @csrf
    <div class="w-full flex flex-wrap -mx-3 mb-6">
        <div class="w-full md:w-1/6 px-3 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="titulo">
                Titulo
            </label>
            <input
                class="appearance-none block w-full bg-gray-200 text-gray-700 border  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                id="titulo" type="text" name="titulo" placeholder="Titulo"
        </div>
        <div class="w-full md:w-1/3 px-3">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="editorial_id"
                Editorial
            </label>
            <div class="relative">
                <select
                    class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    name="editorial_id" id="editorial_id">
                    @foreach ($editoriales as $editorial)
                    <option value="{{$editorial->id}}">{{$editorial->nombre}}</option>
                    @endforeach
                </select>
            </div>

        </div>
        <div class="w-full md:w-1/6 px-3">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="edicion">
                Edicion
            </label>
            <input
                class="appearance-none block w-full bg-gray-200 text-gray-700 border  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                id="edicion" type="number" name="edicion" placeholder="edicion" >

        </div>
    </div>
    <div class="flex flex-wrap -mx-3 mb-6 w-full">
        <label for="pais">pais</label>
        <input type="text" name="pais" id="pais">
        <label for="precio">precio</label>
        <input type="number" name="precio" id="precio">
    </div>
    <div>
        <input type="submit" value="Registrar">
    </div>
</form>

</body>
</html>
