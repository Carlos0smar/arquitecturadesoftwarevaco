<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Editar Libro</h1>

    <form action="{{route('libro.update',$libro->id)}}" method="POST">
        @csrf
        @method('PUT')
        <div>
        <label for="titulo">Titulo</label>
        <input type="text" name="titulo" id="titulo" value="{{$libro->titulo}}">
        <label for="editorial_id">Editorial</label>
        <!-- <input type="number" name="editorial_id" id="editorial_id" value="{{$libro->editorial_id}}"> -->
        <select name="editorial_id" id="editorial_id">
            @foreach ($editoriales as $editorial){
                <option value="{{$editorial->id}}"{{$libro->editorial->id == $editorial->id ? 'selected' : ''}}>
                    {{ $editorial->nombre}}
                </option>
            }
            @endforeach
        </select>
        <label for="edicion">Edicion</label>
        <input type="number" name="edicion" id="edicion" value="{{$libro->edicion}}">
    </div>
    <div>
        <label for="pais">Pais</label>
        <input type="text" name="pais" id="pais" value="{{$libro->pais}}">
        <label for="precio">Precio</label>
        <input type="number" name="precio" id="precio" value="{{$libro->precio}}">
    </div>
    <div>
        <input type="submit" value="Actualizar">
    </div>
    </form>
    
</body>
</html>
    

