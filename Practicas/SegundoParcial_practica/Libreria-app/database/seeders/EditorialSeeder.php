<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Editorial;


class EditorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Editorial::create([
            'id' => 1,
            'nombre' => 'La hoguera'
        ]);
        
        Editorial::create([
            'id' => 2,
            'nombre' => 'Campos'
        ]);

        
        Editorial::create([
            'id' => 3,
            'nombre' => 'La Ramada'
        ]);
    }
}
