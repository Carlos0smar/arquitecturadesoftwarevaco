/*
CRUD CATEGORIES
*/
const router = require('express').Router();
const Libro = require('../models/libro');
const VerifyToken = require('./VerifyToken');


let logger = require('../../config/log');


//SEARCH - Categoria
router.get("/libro/:id", (req, res) => {
   
  const filter = {
    id,
    titulo,
    edicion,
    pais,
    precio
  } = req.query;

Libro.searchLibro( req.params.id, (err, data) => {
    res.json(data);
  });
});


//CREATE - Libro
router.post("/libros", (req, res) => {
  logger.info("Begin insert libro");
  const LibroData = {
    id:null,
    titulo:req.body.titulo,
    edicion:req.body.edicion,
    pais:req.body.pais,
    precio:req.body.precio
  };
  Libro.insertLibro(LibroData, (err, data) =>{
    if(data && data.insertlibro){
      res.json({
        success: true,
        data: data
      })
      logger.info("Libro inserted");
    }
  });
  logger.info("End insert libro");
});

//READ - Libro
router.get("/libros", (req, res) => {
  logger.info("Begin list libro");
  Libro.getLibros((err, data) => {
    res.json(data);
  });
  logger.info("End list libro");
});

//UPDATE - Libro
router.put("/libros/:id", (req, res) => {
  logger.info("Begin update libro");
  const LibroData = {
    id: req.params.id,
    titulo: req.body.titulo,
    edicion: req.body.edicion,
    pais: req.body.pais,
    precio: req.body.precio
  };
  Libro.updateLibro(LibroData, (err, data) => {
    if(data && data.msg){
      res.json(data)
      logger.info("Libro updated");
    }else{
      res.json({
        success: false,
        "msg": "error"
      })
      logger.info("The libro was not updated");
    }
  })
  logger.info("End update libro");
});

//DELETE - Libro
router.delete("/libros/:id", (req, res) => {
  logger.info("Begin delete libro");
  Libro.deleteLibro(req.params.id, (err, data) => {    
    if(data && data.msg === 'deleted'){
      res.json({
        success:true,
        data: data
      })
      logger.info("Libro deleted");
    }else{
      res.status(500).json({
        "msg": "error"
      })
      logger.info("The libro was not deleted");
    }
  });
  logger.info("End delete libro");
});

module.exports = router;