const dbConnection = require('../../config/dbConnection').pool;

let Libro = {};

Libro.getLibros = (callback) => {
  if (dbConnection) {
    dbConnection.query('SELECT * FROM libros l ', (err, rows) => {
      if(err){
        throw err;
      }else{
        callback(null, rows);
      }
    });
  }
};

Libro.insertLibro = (libroData, callback) => {
  if(dbConnection){
    dbConnection.query('INSERT INTO libros SET ?', libroData, (err, result) => {
      if(err){
        throw err;
      }else{
        callback(null, {
          insertId: result.insertID
        })
      }
    })
  }
};

Libro.updateLibro = (libroData, callback) => {
  if (dbConnection) {
    const sql = `
      UPDATE libros SET
      titulo = ${dbConnection.escape(libroData.titulo)},
      edicion = ${dbConnection.escape(libroData.edicion)},
      pais = ${dbConnection.escape(libroData.pais)},
      precio = ${dbConnection.escape(libroData.precio)}
      WHERE id = ${dbConnection.escape(libroData.id)}
      ` 
    dbConnection.query(sql, (err, result) => {
      if (err) {
        throw err;
      } else {
        callback(null, {
          "msg": "success" 
        })
      }
    })
  }
};

Libro.deleteLibro = (id, callback) => {
  if(dbConnection){
    let sql = `SELECT * FROM libros WHERE id = ${dbConnection.escape(id)}`;

    dbConnection.query(sql, (err, row) => {
      if(row){
        let sql = `DELETE FROM libros WHERE id = ${dbConnection.escape(id)}`;
        dbConnection.query(sql, (err, resul) => {
          if(err){
            throw err;
          }else{
            callback(null, {
              "msg": "deleted"
            })
          }
        })
      }else{
        callback(null, {
          "msg": "not exist"
        })
      }
    })
  }
};

Libro.searchLibro = (categoryFilter, callback) => {
  if (dbConnection) {
    
    let findId = categoryFilter;
    let consulta = `
      SELECT *
      FROM libros 
      WHERE id = ${findId}
      `
    let sql;
    if(categoryFilter.limit != null){
      sql = consulta.concat('LIMIT ' + categoryFilter.limit);
    }else{
      sql = consulta;
    }
  
    dbConnection.query(sql, (err, rows) => {
      if (err) {
        throw err;
      } else {
        callback(null, rows);
      }
    })
  }
};

module.exports = Libro;