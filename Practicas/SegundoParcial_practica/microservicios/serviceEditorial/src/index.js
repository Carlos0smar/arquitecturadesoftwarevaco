const express = require('express');
const app = express();
const librosRoutes = require('./app/routes/categoryRoutes');

//settings
app.set('port', process.env.PORT || 3004);

//middlewares

app.use(express.json());


// routes
app.use('/api/v1', librosRoutes);

app.listen(app.get('port'), () => {
    console.log('server on port ', app.get('port'));
});