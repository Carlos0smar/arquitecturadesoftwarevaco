const dbConnection = require('../../config/dbConnection').pool;

let Editorial = {};

Editorial.getEditoriales = (callback) => {
  if (dbConnection) {
    dbConnection.query('SELECT id, nombre FROM editorials ', (err, rows) => {
      if(err){
        throw err;
      }else{
        callback(null, rows);
      }
    });
  }
};

Editorial.insertEditorial = (editorialData, callback) => {
  if(dbConnection){
    dbConnection.query('INSERT INTO editorials SET ?', editorialData, (err, result) => {
      if(err){
        throw err;
      }else{
        callback(null, {
          insertId: result.insertID
        })
      }
    })
  }
};

Editorial.updateEditorial = (editorialData, callback) => {
  if (dbConnection) {
    const sql = `
      UPDATE editorials SET
      nombre = ${dbConnection.escape(editorialData.nombre)}
      WHERE id = ${dbConnection.escape(editorialData.id)}
      ` 
    dbConnection.query(sql, (err, result) => {
      if (err) {
        throw err;
      } else {
        callback(null, {
          "msg": "success" 
        })
      }
    })
  }
};

Editorial.deleteEditorial = (id, callback) => {
  if(dbConnection){
    let sql = `SELECT * FROM editorials WHERE id = ${dbConnection.escape(id)}`;

    dbConnection.query(sql, (err, row) => {
      if(row){
        let sql = `DELETE FROM editorials WHERE id = ${dbConnection.escape(id)}`;
        dbConnection.query(sql, (err, resul) => {
          if(err){
            throw err;
          }else{
            callback(null, {
              "msg": "deleted"
            })
          }
        })
      }else{
        callback(null, {
          "msg": "not exist"
        })
      }
    })
  }
};

Editorial.searchEditorial = (categoryFilter, callback) => {
  if (dbConnection) {
    
    let findId = categoryFilter;
    let consulta = `
      SELECT id, nombre
      FROM editorials 
      WHERE id = ${findId}
      `
    let sql;
    if(categoryFilter.limit != null){
      sql = consulta.concat('LIMIT ' + categoryFilter.limit);
    }else{
      sql = consulta;
    }
  
    dbConnection.query(sql, (err, rows) => {
      if (err) {
        throw err;
      } else {
        callback(null, rows);
      }
    })
  }
};

module.exports = Editorial;