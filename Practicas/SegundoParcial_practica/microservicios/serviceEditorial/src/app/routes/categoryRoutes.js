/*
CRUD CATEGORIES
*/
const router = require('express').Router();
const Editorial = require('../models/editorial');
const VerifyToken = require('./VerifyToken');


let logger = require('../../config/log');


//SEARCH - Categoria
router.get("/editorial/:id", (req, res) => {
   
  const filter = {
    id,
    nombre
  } = req.query;

Editorial.searchEditorial( req.params.id, (err, data) => {
    res.json(data);
  });
});


//CREATE - Editorial
router.post("/editoriales", (req, res) => {
  logger.info("Begin insert editorial");
  const EditorialData = {
    id:null,
    nombre:req.body.nombre,
  };
  Editorial.insertEditorial(EditorialData, (err, data) =>{
    if(data && data.inserteditorial){
      res.json({
        success: true,
        data: data
      })
      logger.info("Editorial inserted");
    }
  });
  logger.info("End insert editorial");
});

//READ - Editorial
router.get("/editoriales", (req, res) => {
  logger.info("Begin list editorial");
  Editorial.getEditoriales((err, data) => {
    res.json(data);
  });
  logger.info("End list editorial");
});

//UPDATE - Editorial
router.put("/editoriales/:id", (req, res) => {
  logger.info("Begin update editorial");
  const EditorialData = {
    id: req.params.id,
    nombre: req.body.nombre
  };
  Editorial.updateEditorial(EditorialData, (err, data) => {
    if(data && data.msg){
      res.json(data)
      logger.info("Editorial updated");
    }else{
      res.json({
        success: false,
        "msg": "error"
      })
      logger.info("The editorial was not updated");
    }
  })
  logger.info("End update editorial");
});

//DELETE - Editorial
router.delete("/editoriales/:id", (req, res) => {
  logger.info("Begin delete editorial");
  Editorial.deleteEditorial(req.params.id, (err, data) => {    
    if(data && data.msg === 'deleted'){
      res.json({
        success:true,
        data: data
      })
      logger.info("Editorial deleted");
    }else{
      res.status(500).json({
        "msg": "error"
      })
      logger.info("The editorial was not deleted");
    }
  });
  logger.info("End delete editorial");
});

module.exports = router;