const mysql = require('mysql');

let pool = mysql.createPool({
    host: "localhost",
    port:"3307",
    user: "root",
    password: "",
    database: 'db_libreria'
});

exports.pool = pool;