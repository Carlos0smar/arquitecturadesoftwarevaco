<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<h1>Listado de las tarifas</h1>
<table>
    <tr>
        <th>Hora</th>
        <th>Precio</th>
        <th>Descripcion</th>    
    </tr>
@foreach ($tarifas as $tarifa)
<tr>
    <td>{{$tarifa->hora}}</td>
    <td>{{$tarifa->precio}}</td>
    <td>{{$tarifa->descripcion}}</td>
    <td>{{$tarifa->fechanacimiento}}</td>
    <td><a href="{{route('tarifa.edit',$tarifa->id)}}">Editar</a></td>
    <td>
        <form action="{{route('tarifa.destroy',$tarifa->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit">Eliminar</button>
        </form>
    </td>
</tr>
@endforeach
</table>    
<a href="{{route('tarifa.create')}}">Crear Tarifa</a>
</body>
</html>