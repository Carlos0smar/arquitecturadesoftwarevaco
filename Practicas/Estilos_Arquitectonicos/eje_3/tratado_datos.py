import pandas as pd
class tratado:
    def __init__(self, data):
        self.data = data
        
    def contiene_letra(data):
        num = ''
        for caracter in data:
            if not  caracter.isalpha():
                num += caracter
        return num 
    
    def contiene_letra_(data):
        for caracter in data:
            if caracter.isalpha():
                return True
        return False

    def contiene_numero(data):
        # Reemplazar valores NaN con un espacio en blanco
        if pd.isna(data):
            return " "
        
        elemento = ''
        if isinstance(data, float):
            data = str(data)
        for caracter in data:
            if caracter.isalpha():
                elemento += caracter
        return elemento



    def cambiar_formato_fecha(data):
        vector = data.split('/')
        return vector[2] + '-' + vector[1] + '-' + vector[0]