instructions = [
    'SET FOREIGN_KEY_CHECKS = 0;',
    'DROP TABLE IF EXISTS user;',
    'DROP TABLE IF EXISTS persona;',
    """
    CREATE TABLE persona(
        ci VARCHAR(9) PRIMARY KEY,
        nombre VARCHAR(100) NOT NULL,
        apellido VARCHAR(100) NOT NULL,
        telefono VARCHAR(10),
        email VARCHAR(100) NOT NULL,
        carrera VARCHAR(100) NOT NULL,
        fecha_registro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    );
    """,
    """
    CREATE TABLE user(
        id INT PRIMARY KEY AUTO_INCREMENT,
        ci VARCHAR(9) NOT NULL,
        password VARCHAR(255) NOT NULL,
        nivel INT DEFAULT 1,
        FOREIGN KEY (ci) REFERENCES persona(ci)
    );
    """
]