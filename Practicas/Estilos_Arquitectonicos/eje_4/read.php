<!DOCTYPE html>
<html>
<head>
  <title>Tabla de Datos</title>
  <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
  <?php
  include("conexion.php");
  $sql = "SELECT ci, nombres, apellidos, fecha FROM ciudadano";
  $resultado = $con->query($sql);

  if ($resultado) {
  ?>
    <table>
      <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Fecha</th>
      </tr>
      <?php while ($row = $resultado->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $row["ci"] . "</td>";
        echo "<td>" . $row["nombres"] . "</td>";
        echo "<td>" . $row["apellidos"] . "</td>";
        echo "<td>" . $row["fecha"] . "</td>";
        echo "</tr>";
      } ?>
    </table>
  <?php
  } else {
    echo "No se encontraron registros.";
  }

  // Cerrar la conexión
  $con->close();
  ?>
</body>
</html>
