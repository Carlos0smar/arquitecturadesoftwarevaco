class Combinatoria_ope:
    def __init__(self, n, p):
        self.n = int(n)
        self.p = int(p)
        self.n_p = self.n - self.p

    def factorial(self, n):
        if n == 0:
            return 1
        else:
            return n * self.factorial(n - 1)
        
    def factorial_p(self, p):
        if p == 0:
            return 1
        else:
            return p * self.factorial(p - 1)
        
    def factorial_n_p(self, n_p):
        if n_p == 0 :
            return 1
        else:
            return n_p * self.factorial_n_p(n_p - 1)
        
    def combinatoria(self):
        return self.factorial(self.n) / (self.factorial_p(self.p) * self.factorial_n_p(self.n_p))