package com.example.rutinas;

public class operaciones {
    private int n ;
    private int p ;
    private int n_p ;

    public operaciones(int n, int p) {
        this.n = n;
        this.p = p;
        this.n_p = n-p;
    }

    public int factoria_n(int n){
        if(n == 0){
            return 1;
        } else {
            return n * factoria_n(n-1);
        }
    }

    public int factoria_p(int p){
        if(p == 0){
            return 1;
        } else {
            return p * factoria_n(p-1);
        }
    }

    public int factoria_n_p(int n_p){
        if(n_p == 0){
            return 1;
        } else {
            return n_p * factoria_n(n_p-1);
        }
    }

    public float combinatoria(){
        return factoria_n(n)/(factoria_p(p) * factoria_n_p(n_p));
    }
}
