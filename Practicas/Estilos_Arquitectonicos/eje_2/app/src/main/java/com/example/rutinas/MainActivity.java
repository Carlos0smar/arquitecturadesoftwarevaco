package com.example.rutinas;

import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button Operacion;
    EditText textN, textX;
    TextView textResul;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Operacion = findViewById(R.id.buttonOperacion_Arqui);
        textN = findViewById(R.id.editTextN);
        textX = findViewById(R.id.editTextX);
        textResul = findViewById(R.id.textViewResultado);

        Operacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n = Integer.parseInt(textN.getText().toString());
                int x = Integer.parseInt(textX.getText().toString());
                operaciones resul = new operaciones( n, x);
                textResul.setText(String.valueOf(resul.combinatoria()));
            }

        });

    }
}