/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package composite;
import java.util.Scanner;
/**
 *
 * @author Carlos Osmar
 */
public class Composite {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Pipoca pipocaPequeña = new Pipoca("Pequeña", 5);
        Pipoca pipocaMediana = new Pipoca("Mediana", 8);
        Pipoca pipocaGrande = new Pipoca("Grande", 10);

        Soda sodaPequeña = new Soda("Pequeña", 4);
        Soda sodaMediana = new Soda("Mediana", 6);
        Soda sodaGrande = new Soda("Grande", 8);

 
        Combo combo1 = new Combo();
        combo1.agregarProducto(pipocaMediana);
        combo1.agregarProducto(pipocaMediana);
        combo1.agregarProducto(sodaMediana);
        combo1.agregarProducto(sodaMediana);

        Combo combo2 = new Combo();
        combo2.agregarProducto(pipocaMediana);
        combo2.agregarProducto(sodaMediana);

        Combo combo3 = new Combo();
        combo3.agregarProducto(pipocaGrande);
        combo3.agregarProducto(pipocaGrande);
        combo3.agregarProducto(sodaMediana);
        combo3.agregarProducto(sodaMediana);
        
        
        System.out.println("Inserte el número según la opción que elija:");
        System.out.println("1.- combo 1");
        System.out.println("2.- combo 2");
        System.out.println("3.- combo 3");
        System.out.println("4.- pipoca pequeña");
        System.out.println("5.- pipoca mediana");
        System.out.println("6.- pipoca grande");
        System.out.println("7.- soda pequeña");
        System.out.println("8.- soada mediana");
        System.out.println("9.- soada grande");
         
        int opcion = scanner.nextInt();
        
         switch (opcion) {
            case 1:
                System.out.println("Precio Combo 1: " + combo1.getPrecio() + "bs");
                break;
            case 2:
                System.out.println("Precio Combo 2: " + combo2.getPrecio() + "bs");
                break;
            case 3:
                System.out.println("Precio Combo 3: " + combo3.getPrecio() + "bs");
                break;
            case 4:
                System.out.println("Pipca pequeña: " + pipocaPequeña.getPrecio() + "bs");
                break;
            case 5:
                System.out.println("Pipoca mediana: " + pipocaMediana.getPrecio() + "bs");
                break;
            case 6:
                System.out.println("Pipoca Grande: " + pipocaGrande.getPrecio() + "bs");
                break;
            case 7:
                System.out.println("Soda Pequeña: " + sodaPequeña.getPrecio() + "bs");
                break;
            case 8:
                System.out.println("Soda Mediana: " + sodaMediana.getPrecio() + "bs");
                break;    
            case 9:
                System.out.println("Soda Grande: " + sodaGrande.getPrecio() + "bs");
                break;
            default:
                System.out.println("Opción no válida");
        }

    }
    
}
