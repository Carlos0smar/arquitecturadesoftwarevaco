/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package composite;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Carlos Osmar
 */
public class Combo implements ICandibar {
    
    private List<ICandibar> productos;

    public Combo() {
        productos = new ArrayList<>();
    }

    public void agregarProducto(ICandibar producto) {
        productos.add(producto);
    }

    @Override
    public double getPrecio() {
        double precioTotal = 0;
        for (ICandibar producto : productos) {
            precioTotal += producto.getPrecio();
        }
        return precioTotal;
    }
    
}
