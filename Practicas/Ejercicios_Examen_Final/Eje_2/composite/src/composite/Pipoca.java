/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package composite;

/**
 *
 * @author Carlos Osmar
 */
public class Pipoca implements ICandibar{
    private String tamaño;
    private double precio;

    public Pipoca(String tamaño, double precio) {
        this.tamaño = tamaño;
        this.precio = precio;
    }

    @Override
    public double getPrecio() {
        return precio;
    }
}
