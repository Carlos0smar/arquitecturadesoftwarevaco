<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class soda extends Model
{
    use HasFactory;
    protected $fillable = [
        'tamano',
        'precio'
    ];
}
