<?php

namespace App\Http\Controllers;

use App\Models\Peliculas;
use Illuminate\Http\Request;

class PeliculasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $peliculas = Peliculas::all();

        return view('peliculas.index', [
            'peliculas' => $peliculas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('peliculas.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
           // dd($request);
        $input = Peliculas::create($request->all());
        if($input){

            return redirect()->route('pelicula.index');
        }else{
            dd($request);

        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Peliculas $peliculas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Peliculas $peliculas)
    {
        return view('peliculas.edit',[
            'pelicula' => $peliculas
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Peliculas $peliculas)
    {
        $input = $request->all();
        // dd($input);
        $peliculas->update($input);
        
        return redirect()->route('pelicula.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Peliculas $peliculas)
    {
        // dd($peliculas);
        $peliculas->delete();
        return redirect()->route('pelicula.index');
    }
}
