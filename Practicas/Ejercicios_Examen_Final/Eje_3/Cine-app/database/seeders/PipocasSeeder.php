<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\pipocas;

class PipocasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        pipocas::factory(20)->create();
    }
}
