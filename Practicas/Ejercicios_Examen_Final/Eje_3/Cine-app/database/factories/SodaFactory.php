<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\soda>
 */
class SodaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $tamanos = ['pequeno','mediano', 'grande']; 
        $precios = [
            'pequeno' =>4,
            'mediano' =>6,
            'grande' =>8,
        ]; 

        $tamano = fake()->randomElement($tamanos);
        $precio = $precios[$tamano]; 
        return [
            'tamano'=> $tamano,
            'precio'=> $precio
        ];
    }
}
