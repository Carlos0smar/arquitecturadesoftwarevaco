<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Peliculas>
 */
class PeliculasFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {   
        $lista = [];

        for($i=0; $i<=5 ; $i++){
            $character = fake()->firstName() .' '. fake()->lastName();
            $lista[$i]= $character;
        }
        $artistas_principales = $lista[0].', '.$lista[1].', '.$lista[2].', '.$lista[3].', '.$lista[4].', '.$lista[5];
        return [
            'titulo' =>fake()->word(),
            'artistas_principales'=>$artistas_principales,
            'anio'=>fake()->year(),
            'productora'=>fake()->firstName() . ' '. fake()->lastName(),
            'genero'=>fake()->word()
        ];
    }
}
