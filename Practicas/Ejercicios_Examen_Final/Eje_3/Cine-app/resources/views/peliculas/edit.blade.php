<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Editar pelicula</h1>

    <form action="{{route('pelicula.update', $pelicula->id)}}" method="POST">
        @csrf
        @method('PUT')
        <div>
        <label for="titulo">Titulo</label>
        <input type="text" name="titulo" id="titulo" value="{{$pelicula->titulo}}">
        <label for="artistas_principales">Artistas Prinicpales</label>
        <input type="text" name="artistas_principales" id="artistas_principales" value="{{$pelicula->artistas_principales}}">
        <label for="anio">Año</label>
        <input type="number" name="anio" id="anio" value="{{$pelicula->anio}}">
    </div>
    <div>
        <label for="productora">Productora</label>
        <input type="text" name="productora" id="productora" value="{{$pelicula->productora}}">
        <label for="genero">Genero</label>
        <input type="text" name="genero" id="genero" value="{{$pelicula->genero}}">
    </div>
    <div>
        <input type="submit" value="Actualizar">
    </div>
    </form>
    
</body>
</html>
    

