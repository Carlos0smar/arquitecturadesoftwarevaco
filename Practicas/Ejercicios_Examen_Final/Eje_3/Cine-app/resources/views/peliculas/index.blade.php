<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<h1>Listado de Peliculas</h1>
<div class="flex flex-col">
    <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
        <div class="overflow-hidden">
          <table class="min-w-full text-left text-sm font-light">
            <thead class="border-b font-medium">
              <tr>
                <th scope="col" class="px-6 py-4">Titulo</th>
                <th scope="col" class="px-6 py-4">artistas principales</th>
                <th scope="col" class="px-6 py-4">año</th>    
                <th scope="col" class="px-6 py-4">productora</th>
                <th scope="col" class="px-6 py-4">gemero</th>

              </tr>
            </thead>
            <tbody>
                @foreach ($peliculas as $pelicula)
                <tr class="border-b">
                    <td class="whitespace-nowrap px-6 py-4">{{$pelicula->titulo}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$pelicula->artistas_principales}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$pelicula->anio}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$pelicula->productora}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$pelicula->genero}}</td>
                    <td><a href="{{route('pelicula.show',$pelicula->id)}}">Mostrar</a>
                        <a href="{{route('pelicula.edit',$pelicula->id)}}">Editar</a></td>
                    <td>
                        <form action="{{route('pelicula.destroy',$pelicula->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit">Eliminar</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>  
</body>
</html>


