<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<form class="w-full " action="{{route('pelicula.store')}}" method="POST">
    @csrf
    <div class="w-full flex flex-wrap -mx-3 mb-6">
        <div class="w-full md:w-1/6 px-3 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="titulo">
                Titulo
            </label>
            <input
                class="appearance-none block w-full bg-gray-200 text-gray-700 border  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                id="titulo" type="text" name="titulo" placeholder="Titulo">
        </div>
        <div class="w-full md:w-1/3 px-3">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="editorial_id">
                Artistas Principales
            </label>
            <input
                class="appearance-none block w-full bg-gray-200 text-gray-700 border  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                id="artistas_principales" type="text" name="artistas_principales" placeholder="artistas_principales">
            </div>

        </div>
        <div class="w-full md:w-1/6 px-3">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="anio">
                Año
            </label>
            <input
                class="appearance-none block w-full bg-gray-200 text-gray-700 border  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                id="anio" type="number" name="anio" placeholder="anio" >

        </div>
    </div>
    <div class="flex flex-wrap -mx-3 mb-6 w-full">
        <label for="productora">Productora</label>
        <input type="text" name="productora" id="productora">
        <label for="genero">Genero</label>
        <input type="text" name="genero" id="genero">
    </div>
    <div>
        <input type="submit" value="Registrar">
    </div>
</form>

</body>
</html>
