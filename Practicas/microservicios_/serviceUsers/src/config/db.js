// const mongoose = require('mongoose');
// mongoose.connect('mongodb://localhost:27017/bd_usuarios');


const mongoose = require('mongoose');

// Función asincrónica para conectar a la base de datos
async function connectToDatabase() {
  try {
    await mongoose.connect('mongodb://127.0.0.1:27017/bd_usuarios', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log('Conexión exitosa a la base de datos');
  } catch (error) {
    console.error('Error al conectar a la base de datos:', error);
  }
}

// Llama a la función asincrónica para iniciar la conexión
connectToDatabase();