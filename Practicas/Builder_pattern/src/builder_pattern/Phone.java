/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package builder_pattern;


/**
 *
 * @author Carlos Osmar
 */
public class Phone {
    
    public TipoProcesador tipoprocesador;
    public boolean plegable;
    public CantidadDeRam ram;
    public CantidadDeAlmacenamiento almacenamiento;
    public Bateria bateria;
    public Marca marca;
    public Modelo modelo;
    public Color color;
    public Tamano tamano;

    public void Mostrar() {
        System.out.println("Celular con procesador:  " + tipoprocesador + " ");
        System.out.println("Canmtidad de ram: " + ram);
        System.out.println("La batería tiene de capacidad:"+ bateria);
        System.out.println("La Marca del celular es : " + marca);
        System.out.println("El modleo del celular es : " + modelo);
        System.out.println("Color del celular: " + color);
        System.out.println("Tamaño del celular es: " + tamano);

        
        if (plegable) {
            System.out.println("Pantalla Plegabel");
        }
    }
}

enum TipoProcesador {
    Snapdragon855,	
    AppleA17Bionic,
    Snapdragon8Gen2
}

enum  CantidadDeRam {
    _8GB,
    _6GB,
    _12GB
}

enum CantidadDeAlmacenamiento {
    _256GB,
    _512B
}


enum Bateria {
    _3300mAh,
    _3349mAh,	
    _4820mAh
}

enum Marca{
    Samsung,
    IPhone,
    Xiaomi
}

enum Modelo{
    iphone15,
    samsung_flip,
    xiaomi13pro
}

enum Tamano{
    _14cmx7cm,
    _162x74x8mm,
desplegado_167x73x7mm_plegado_87x73x15,

    
}
enum Color{
    negro, 
    blanco,
    dorado,
    azul,
    plateado
}

