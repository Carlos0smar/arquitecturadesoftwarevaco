/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package builder_pattern;

/**
 *
 * @author Carlos Osmar
 */
public class iphone15Builder extends PhoneBuilder {

    @Override
    public void addDesign() {
        
        phone.color = Color.negro;
        phone.plegable = false;
        phone.tamano = Tamano._14cmx7cm;
    }

    @Override
    public void addIdetifier() {
        phone.marca=Marca.IPhone;
        phone.modelo = Modelo.iphone15;
        
    }

    @Override
    public void addComponents() {
        phone.ram = CantidadDeRam._6GB;
        phone.tipoprocesador = TipoProcesador.AppleA17Bionic;
        phone.almacenamiento = CantidadDeAlmacenamiento._256GB;
    }

    @Override
    public void addBattery() {
        phone.bateria = Bateria._3300mAh;
    }
    
}
