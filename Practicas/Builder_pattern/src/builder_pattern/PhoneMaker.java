/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package builder_pattern;

/**
 *
 * @author Carlos Osmar
 */
public class PhoneMaker {
    
        private PhoneBuilder builder;

       public PhoneMaker(PhoneBuilder builder)
       {
           this.builder = builder;
       }

       public void BuildPhone()
       {
           builder.createPhone();
           builder.addDesign();
           builder.addIdetifier();
           builder.addComponents();
           builder.addBattery();
           
       }

       public Phone getPhone()
       {
           return builder.getPhone();
       }
    
}
