/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package builder_pattern;

/**
 *
 * @author Carlos Osmar
 */
public class xiaomi13proBuilder extends PhoneBuilder{
    
    @Override
    
    public void addDesign() {
        
        phone.color = Color.plateado;
        phone.plegable = false;
        phone.tamano = Tamano.desplegado_167x73x7mm_plegado_87x73x15;
    }

    @Override
    public void addIdetifier() {
        phone.marca=Marca.Xiaomi;
        phone.modelo = Modelo.xiaomi13pro;
        
    }

    @Override
    public void addComponents() {
        phone.ram = CantidadDeRam._12GB;
        phone.tipoprocesador = TipoProcesador.Snapdragon8Gen2;
        phone.almacenamiento = CantidadDeAlmacenamiento._512B;
    }

    @Override
    public void addBattery() {
        phone.bateria = Bateria._4820mAh;
    }
    
}
