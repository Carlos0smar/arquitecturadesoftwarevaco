/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package builder_pattern;

/**
 *
 * @author Carlos Osmar
 */
public class Builder_pattern {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        PhoneMaker fabricandoIphone = new PhoneMaker(new iphone15Builder());
        fabricandoIphone.BuildPhone();
        Phone phoneIphone = fabricandoIphone.getPhone();
        phoneIphone.Mostrar();

        PhoneMaker fabricandoSamsung = new PhoneMaker(new samsung_flipBuilder());
        fabricandoSamsung.BuildPhone();
        Phone phoneSamsung = fabricandoSamsung.getPhone();
        phoneSamsung.Mostrar();

        PhoneMaker fabricandoXiaomi = new PhoneMaker(new xiaomi13proBuilder());
        fabricandoXiaomi.BuildPhone();
        Phone phoneXiaomi = fabricandoXiaomi.getPhone();
        phoneXiaomi.Mostrar();
    }
    
}
