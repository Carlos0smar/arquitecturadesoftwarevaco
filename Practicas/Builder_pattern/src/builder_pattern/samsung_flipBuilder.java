/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package builder_pattern;

/**
 *
 * @author Carlos Osmar
 */
public class samsung_flipBuilder extends PhoneBuilder{
    
    @Override
    public void addDesign() {
        
        phone.color = Color.azul;
        phone.plegable = true;
        phone.tamano = Tamano._162x74x8mm;
    }

    @Override
    public void addIdetifier() {
        phone.marca=Marca.Samsung;
        phone.modelo = Modelo.samsung_flip;
        
    }

    @Override
    public void addComponents() {
        phone.ram = CantidadDeRam._8GB;
        phone.tipoprocesador = TipoProcesador.Snapdragon855;
        phone.almacenamiento = CantidadDeAlmacenamiento._256GB;
    }

    @Override
    public void addBattery() {
        phone.bateria = Bateria._3349mAh;
    }
    
}
