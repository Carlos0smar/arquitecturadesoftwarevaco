/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package builder_pattern;

/**
 *
 * @author Carlos Osmar
 */
abstract class PhoneBuilder {
    
    protected  Phone phone;

       public Phone getPhone()
       {
           return phone;
       }

       public void createPhone()
       {
            phone = new Phone();
       }

       public abstract void addDesign();
       public abstract void addIdetifier();
       public abstract void addComponents();
       public abstract void addBattery();

}
