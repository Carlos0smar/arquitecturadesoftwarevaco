<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class destinatario extends Model
{
    use HasFactory;
    protected $fillable =[
        'nombre',
        'cargo'
    ];
    public function correspondencia() {
        return $this->hasMany(Correspondencia::class,'destinatario_id' , 'id');
    }
}
