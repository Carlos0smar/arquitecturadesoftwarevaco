<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Correspondencia extends Model
{
    use HasFactory;
    protected $fillable =[
        'fecha',
        'remitente',
        'asunto',
        'cita',
        'destinatario_id'
    ];

    
    public function destinatario()
    {
        return $this->belongsTo(destinatario::class,'destinatario_id' , 'id');
    }
}
