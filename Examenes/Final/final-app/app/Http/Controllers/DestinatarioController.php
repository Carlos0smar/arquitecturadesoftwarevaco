<?php

namespace App\Http\Controllers;

use App\Models\destinatario;
use Illuminate\Http\Request;

class DestinatarioController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(destinatario $destinatario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(destinatario $destinatario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, destinatario $destinatario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(destinatario $destinatario)
    {
        //
    }
}
