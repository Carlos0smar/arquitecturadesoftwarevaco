<?php

namespace App\Http\Controllers;

use App\Models\Correspondencia;
use App\Models\destinatario;
use Illuminate\Http\Request;

class CorrespondenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $correspondencias = Correspondencia::all();

        return view('Correspondencia.index', [
            'correspondencias' => $correspondencias
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Correspondencia $correspondencia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Correspondencia $correspondencium)
    {
        $destinatarios=destinatario::all();
        return view('correspondencia.edit',[
            'correspondencia' => $correspondencium, 'destinatarios'=>$destinatarios
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Correspondencia $correspondencia)
    {
        $input = $request->all();
        // dd($input);
        $correspondencia->update($input);
        
        return redirect()->route('correspondencia.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Correspondencia $correspondencia)
    {
        //
    }
}
