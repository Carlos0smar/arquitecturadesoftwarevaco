<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Correspondencia>
 */
class CorrespondenciaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $maxNbChars = 100;
        return [
            'fecha' => fake()->date(),
            'remitente' => fake()->name(),
            'destinatario_id' => fake()->numberBetween(1,3),
            'asunto' => fake()->realText(100),
            'cita' => fake()->randomNumber(2,false)
        ];
    }
}
