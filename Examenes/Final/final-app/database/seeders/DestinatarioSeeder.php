<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\destinatario;

class DestinatarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        destinatario::create([
            'id' => 1,
            'nombre' => 'Jose rojas',
            'cargo' => 'Gerente'

        ]);
        
        destinatario::create([
            'id' => 2,
            'nombre' => 'Pedro Campos',
            'cargo' => 'Administrador'
        ]);

        
        destinatario::create([
            'id' => 3,
            'nombre' => 'Miranda Arispe',
            'cargo' => 'Personal'
        ]);
    }
}
