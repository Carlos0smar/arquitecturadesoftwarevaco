<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Editar Libro</h1>

    <form action="{{route('correspondencia.update',$correspondencia->id)}}" method="POST">
        @csrf
        @method('PUT')
        <div>
        <label for="fecha">fecha</label>
        <input type="date" name="fecha" id="fecha" value="{{$correspondencia->fecha}}">
        <label for="destinatario_id">destinatario</label>
        <!-- <input type="number" name="destinatario_id" id="destinatario_id" value="{{$correspondencia->destinatario_id}}"> -->
        <select name="destinatario_id" id="destinatario_id">
            @foreach ($destinatarios as $destinatario){
                <option value="{{$destinatario->id}}"{{$correspondencia->destinatario->id == $destinatario->id ? 'selected' : ''}}>
                    {{ $destinatario->nombre}}
                </option>
            }
            @endforeach
        </select>
        <label for="remitente">remitente</label>
        <input type="text" name="remitente" id="remitente" value="{{$correspondencia->remitente}}">
    </div>
    <div>
        <label for="asunto">asunto</label>
        <input type="text" name="asunto" id="asunto" value="{{$correspondencia->asunto}}">
        <label for="cita">cita</label>
        <input type="number" name="cita" id="cita" value="{{$correspondencia->cita}}">
    </div>
    <div>
        <input type="submit" value="Actualizar">
    </div>
    </form>
    
</body>
</html>
    

