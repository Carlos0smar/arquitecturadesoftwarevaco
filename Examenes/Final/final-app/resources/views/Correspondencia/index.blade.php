<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<h1>Listado de clientes</h1>
<div class="flex flex-col">
    <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
        <div class="overflow-hidden">
          <table class="min-w-full text-left text-sm font-light">
            <thead class="border-b font-medium">
              <tr>
                <th scope="col" class="px-6 py-4">fecha</th>
                <th scope="col" class="px-6 py-4">remitente</th>
                <th scope="col" class="px-6 py-4">asunto</th>    
                <th scope="col" class="px-6 py-4">cita</th>
                <th scope="col" class="px-6 py-4">destinatario</th>

              </tr>
            </thead>
            <tbody>
                @foreach ($correspondencias as $correspondencia)
                <tr class="border-b">
                    <td class="whitespace-nowrap px-6 py-4">{{$correspondencia->fecha}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$correspondencia->remitente}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$correspondencia->asunto}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$correspondencia->cita}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$correspondencia->destinatario->nombre}}</td>
                    <td>
                        <a href="{{route('correspondencia.edit',$correspondencia->id)}}">Editar</a></td>
                    <td>
                    <!-- <td><a href="{{route('correspondencia.show',$correspondencia->id)}}">Mostrar</a>
                        <a href="{{route('correspondencia.edit',$correspondencia->id)}}">Editar</a></td>
                    <td> -->
                        <!-- <form action="{{route('correspondencia.destroy',$correspondencia->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit">Eliminar</button>
                        </form> -->
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>  
</body>
</html>


