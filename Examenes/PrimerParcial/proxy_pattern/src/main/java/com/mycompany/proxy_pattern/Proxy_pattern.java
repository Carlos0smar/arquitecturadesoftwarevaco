/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.proxy_pattern;

/**
 *
 * @author carlos
 */
public class Proxy_pattern {

    public static void main(String[] args) {
        
        String usuario = "Carlos";
        
        LibroReal libro = new LibroReal("LibroFiccion","Miguel",2000,"content");
        ProxyLibro LibroProxy = new ProxyLibro(libro, usuario);
        LibroProxy.leer();
 
    }
}
