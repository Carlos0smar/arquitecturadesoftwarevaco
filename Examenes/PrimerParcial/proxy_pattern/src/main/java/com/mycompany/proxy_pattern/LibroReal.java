/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.proxy_pattern;

/**
 *
 * @author carlos
 */
public class LibroReal implements ILibro {
    
    public String titulo;
    public String autor;
    public int ano;
    public String contenido;
    
    public LibroReal(String titulo_libro, String autor, int ano, String contenido){
        this.titulo = titulo_libro;
        this.ano = ano;
        this.autor = autor;
        this.contenido = contenido;
    }
    
    @Override
    public void leer() {
        System.out.println("Libro: " + titulo);
        System.out.println(contenido);
    }
    
}
