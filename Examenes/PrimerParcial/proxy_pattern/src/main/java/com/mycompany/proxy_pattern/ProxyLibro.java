/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.proxy_pattern;

/**
 *
 * @author carlos
 */
public class ProxyLibro implements ILibro{

    
    private ILibro libro;
    private String user;
    
    public ProxyLibro(ILibro libro, String usuario) {
        this.libro = libro;
        this.user = usuario;
    }
    

    @Override
    public void leer() {
        System.out.println("Verificando permisos");
        if (this.user == "Carlos"){
            System.out.println("Usuario Autorizado");
            libro.leer();
        }else{
            System.out.println("Usuario no Autorizado");
        }
    } 
    
}
