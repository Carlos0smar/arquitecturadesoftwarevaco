<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Libro>
 */
class LibroFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $especialidades = ['Bolivia', 'Guatemala', 'Chile', 'Argentina', 'Colombia', 'Paraguay'];
        return [
            'titulo' => fake()->firstName(),
            'edicion' => fake()->randomNumber(),
            'pais' => fake()->randomElement($especialidades),
            'precio' => fake()->randomNumber(),
        ];
    }
}
