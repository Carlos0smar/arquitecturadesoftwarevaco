<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    use HasFactory;
    protected $fillable =[
        'titulo',
        'editorial_id',
        'editcion',
        'pais',
        'precio',
        
        $table->foreign('editorial_id')->references('id')->on('editorial')->name('fk_editorial_id')
    ];

    public function editorial()
    {
        return $this->belongsTo(Editorial::class,'editorial_id' , 'id');
    }
}
